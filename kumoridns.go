/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"context"
	"fmt"
	"net"
	"strings"

	"github.com/coredns/coredns/plugin"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"

	"github.com/miekg/dns"
)

// Define log to be a logger with the plugin name in it. This way we can just
// use log.Info and friends to log.
var log = clog.NewWithPlugin("kumoridns")

// KumoriDNS is an plugin implementing the Kumori Service Discovery
type KumoriDNS struct {
	Next       plugin.Handler
	translator *Translator
}

// ServeDNS implements the plugin.Handler interface. This method gets called
// when kumoridns is used in a Server.
func (k KumoriDNS) ServeDNS(
	ctx context.Context, w dns.ResponseWriter, r *dns.Msg,
) (
	int, error,
) {
	meth := "KumoriDNS.ServeDNS()"
	log.Infof("%s ", meth)

	// Request contains some connection state and is useful in plugin.
	state := request.Request{W: w, Req: r}
	log.Infof("%s state.FQDN: %s", meth, state.Name())
	log.Infof("%s state.TYPE: %s", meth, state.Type())

	// Translate the dns name, sending it to Translator via channel, and waiting
	// the response via channel
	oldName := state.Req.Question[0].Name
	k.translator.RequestChn <- oldName
	newName := <-k.translator.ResponseChn
	log.Infof("%s translation result: %s", meth, newName)

	if strings.HasPrefix(newName, "127") {
		//
		// Case 1: a local IP address, which corresponds to a sidecar, has ben returned.
		// So a DNS response must be returned.
		//
		dnsMessage, err := createDNSResponse(state, r, newName)
		if err != nil {
			log.Warningf("%s Error creating DNS message: %s", meth, err.Error())
			return dns.RcodeServerFailure, nil
		} else {
			w.WriteMsg(dnsMessage)
			return dns.RcodeSuccess, nil
		}
	} else if newName != "" {
		//
		// Case 2: a new name has been returned (a translation), so send it to the
		// next plugin
		//

		// Rewrite name
		wr := NewResponseWriterRevert(w, r, oldName)
		state.Req.Question[0].Name = newName

		// Once the name has been rewritten, go to the next plugin
		return plugin.NextOrFailure(k.Name(), k.Next, ctx, wr, r)
	} else {
		//
		// Case 3: no response has been returned.
		// Just go to the next plugin
		//
		return plugin.NextOrFailure(k.Name(), k.Next, ctx, w, r)
	}
}

// Name implements the Handler interface.
func (k KumoriDNS) Name() string {
	return "kumoridns"
}

// Ready implements the ready.Readiness interface, once this flips to true CoreDNS
// assumes this plugin is ready for queries; it is not checked again.
func (k KumoriDNS) Ready() bool {
	meth := "Ready()"
	log.Infof("%s ", meth)
	return true
}

// createDNSResponse creates a DNS response
func createDNSResponse(
	state request.Request, r *dns.Msg, responseStr string,
) (
	msg *dns.Msg, err error,
) {
	//
	// DNS record format (ascii): <name> <ttl> <class> <type> <rdlength> <radata>
	// - name: domain name
	// - ttl: in seconds
	// - class: always "IN" ("internet")
	// - type: A, AAAA, SRV, CNAME, ...
	// - rdlength: optional - size of rdata
	// - rdata: type dependent:
	//   - IPAddress, for A/AAAA
	//   - FQDN, for CNAME
	//   - Priority+weigth+port+FQDN, for SRV
	// Examples:
	// - A:     www.example.com.       3600 IN A     93.184.216.34
	// - AAAA : www.example.com.       3600 IN AAAA  2606:2800:220:1:248:1893:25c8:1946
	// - CNAME: www.example.com.       3600 IN CNAME www.example.net.
	// - SRV:   _sip._tcp.example.com. 3600 IN SRV   10 60 5060 bigbox.example.com.
	//
	// CoreDNS manages DNS records using:
	// https://github.com/miekg/dns
	//
	// type Msg struct {
	// 	MsgHdr
	// 	Compress bool       `json:"-"` // If true, the message will be compressed when converted to wire format.
	// 	Question []Question // Holds the RR(s) of the question section.
	// 	Answer   []RR       // Holds the RR(s) of the answer section.
	// 	Ns       []RR       // Holds the RR(s) of the authority section.
	// 	Extra    []RR       // Holds the RR(s) of the additional section.
	// }
	//
	// RR is an interface for all type of resource records:
	//
	// type RR interface {
	// 	// Header returns the header of an resource record. The header contains
	// 	// everything up to the rdata.
	// 	Header() *RR_Header
	// 	// String returns the text representation of the resource record.
	// 	String() string
	// 	// contains filtered or unexported methods
	// }
	//
	// RR can be instatiated a A/AAAA/SRV records (and more), all of them
	// implementing the String() method
	//
	// type A struct {
	// 	Hdr RR_Header
	// 	A   net.IP `dns:"a"`
	// }
	// type AAAA struct {
	// 	Hdr  RR_Header
	// 	AAAA net.IP `dns:"aaaa"`
	// }
	// type SRV struct {
	// 	Hdr      RR_Header
	// 	Priority uint16
	// 	Weight   uint16
	// 	Port     uint16
	// 	Target   string `dns:"domain-name"`
	// }

	msg = new(dns.Msg)
	msg.SetReply(r)          // SetReply creates a reply message from a request message.
	msg.Authoritative = true // not delegated

	// Search qname and qtype in the zone
	// Three sets of records are returned:
	// - one for the answer (m.Answer), >> we use this (A, AAAA, SRV records -- but
	//   we use only A records, for the sidecar local IP),
	// - one for authority (m.Ns), >> we dont use this
	// - one for the additional section (m.Extra) >> we dont use this
	answers := []dns.RR{}
	qname := state.Name()
	switch state.QType() {
	case dns.TypeA:
		dnsA := createA(qname, responseStr)
		answers = append(answers, dnsA)
	case dns.TypeAAAA:
		// For now, only A type is allowed
		// Example of AAAA type answer:
		// dnsAAAA := createAAAA(qname, "0:0:0:0:0:ffff:7f00:a")
		// answers = append(answers, dnsAAAA)
		err = fmt.Errorf("Error creating dns response: type AAAA now allowed")
	case dns.TypeSRV:
		// For now, only A type is allowed
		// Example of SRV type answer:
		// dnsSRV := createSRV(qname, "myservice1.kumori.systems.", 5601)
		// answers = append(answers, dnsSRV)
		err = fmt.Errorf("Error creating dns response: type SRV now allowed")
	default:
		err = fmt.Errorf("Error creating dns response: unknown type %s", state.Type())
	}

	msg.Answer = answers
	return
}

func createA(qname string, IP string) (dnsA *dns.A) {
	dnsA = new(dns.A)
	dnsA.Hdr = dns.RR_Header{
		Name:   qname,
		Rrtype: dns.TypeA,
		Class:  dns.ClassINET,
		Ttl:    30,
	}
	dnsA.A = net.ParseIP(IP)
	return dnsA
}

// NOT USED!
// func createAAAA(qname string, IP string) *dns.AAAA {
// 	r := new(dns.AAAA)
// 	r.Hdr = dns.RR_Header{
// 		Name:   qname,
// 		Rrtype: dns.TypeAAAA,
// 		Class:  dns.ClassINET,
// 		Ttl:    3600,
// 	}
// 	r.AAAA = net.ParseIP(IP)
// 	return r
// }

// NOT USED!
// func createSRV(qname string, IP string, port uint16) *dns.SRV {
// 	r := new(dns.SRV)
// 	r.Hdr = dns.RR_Header{
// 		Name:   qname,
// 		Rrtype: dns.TypeSRV,
// 		Class:  dns.ClassINET,
// 		Ttl:    3600,
// 	}
// 	r.Priority = 0
// 	r.Weight = 100
// 	r.Port = port
// 	r.Target = IP
// 	return r
// }
