################################################################################
##  KUMORI WARNING: This Dockerfile is used by CI processes, so any changes   ##
##                  should make sure the CI pipelines don't break.            ##
################################################################################

#
# STEP 1: COMPILATION
#

# Build the manager binary
# Original CoreDNS v1.9.3 uses golang v1.17 for compilation, but we use golang v1.19
# because the Kumori client-go requires that version. +
FROM golang:1.19 as builder

# These variables are used by CI to clean up intermediate stage images after
# image generation.
ARG DELETE_LABEL=default
ARG CI_JOB_TOKEN
LABEL deletemark="${DELETE_LABEL}"
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace

RUN apt-get update && apt-get -uy upgrade
RUN apt-get -y install ca-certificates && update-ca-certificates

# Allow access to kumori gitlab
RUN mkdir -p /root/.ssh
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com 2> /dev/null | tee -a /root/.ssh/known_hosts > /dev/null 2>&1
RUN echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > /root/.netrc
RUN cat /root/.netrc

# Optionally add a private key for accessing private Git repositories (mostly
# for DEV or private dependencies: for example a private kumori client-go package).
# You must copy some files into the workspace directory, BEFORE execute docker build:
#   - cp  $HOME/.ssh/ecloud_deployment_key .
#   - cp  $HOME/.gitconfig .
#
# COPY ecloud_deployment_key /root/.ssh/id_rsa
# RUN chmod 0600 /root/.ssh/id_rsa
# COPY .gitconfig /root/.gitconfig

# Copy the go source
COPY / .
COPY /gitlab-ci-helper /gitlab-ci-helper

# kumoridns is not a complete GO project, so it can be compiled!.
# This plugin must be included in the compilation of CoreDNS... so coredns
# must be prepared first (add kumoridns plugin as "in-tree" plugin)
# CoreDNS project is prepared in ./coredns directory
RUN gitlab-ci-helper/prepare-coredns-project.sh

# Build, using the Makefile included in coredns project
WORKDIR coredns
RUN make

#
# STEP 2: IMAGE GENERATION
#
# Which base image to use?
#
# Original CoreDNS uses:
#   FROM scratch
# But FOR NOW we use:
#   FROM gcr.io/distroless/static
#
# Why? Docker v17 (used in our gitlab-runners FOR NOW) has a bug. When "scratch"
# is used in multistage, the second image (derived from scratch) doesn't reset
# the metadata fixed in the previous stages.
# https://github.com/moby/moby/pull/33179
#
# As soon as posible we will use Docker v19 in our gitlab runners.
#

FROM gcr.io/distroless/static
#FROM scratch

WORKDIR /
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /workspace/coredns/coredns .
EXPOSE 53 53/udp

ENTRYPOINT ["/coredns"]
