/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"encoding/json"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	corev1informers "k8s.io/client-go/informers/core/v1"
	clientset "k8s.io/client-go/kubernetes"
	corev1listers "k8s.io/client-go/listers/core/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

//
// ServiceController detects changes in the Kubernetes services, and sends
// (via go channel) relevant information to the Translation table.
// What is that relevant information? The content of the "connector" annotation,
// included in each service (see connector.go)
//

const kindKubeService = "Service"

// ServiceController is the struct representing the controller
type ServiceController struct {
	BaseController
	kubeClientset   clientset.Interface
	serviceInformer corev1informers.ServiceInformer
	serviceLister   corev1listers.ServiceLister
	namespace       string
	msgChn          chan ConnectorMessage
	threadiness     int
}

// NewServiceController creates a new controller for kube-services, used in the
// kumoridns plugin (coredns plugin)
func NewServiceController(
	kubeClientset clientset.Interface,
	serviceInformer corev1informers.ServiceInformer,
	namespace string,
	msgChn chan ConnectorMessage,
	threadiness int,
) (s *ServiceController) {

	controllerName := "kumoridnsservicecontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 1)
	syncedFunctions[0] = serviceInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = serviceInformer.Informer()

	serviceLister := serviceInformer.Lister()

	baseController := BaseController{
		Name:                   controllerName,
		Kind:                   kindKubeService,
		Workqueue:              workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		SyncedFunctions:        syncedFunctions,
		PrimarySharedInformers: primarySharedInformers,
	}

	s = &ServiceController{
		BaseController:  baseController,
		kubeClientset:   kubeClientset,
		serviceInformer: serviceInformer,
		serviceLister:   serviceLister,
		namespace:       namespace,
		msgChn:          msgChn,
		threadiness:     threadiness,
	}
	s.BaseController.IBaseController = s
	return
}

// SyncHandler ...
func (s *ServiceController) SyncHandler(key string) error {
	meth := s.Name + ".SyncHandler() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		err = fmt.Errorf("Invalid resource key: %s", key)
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	if s.namespace != "" && s.namespace != namespace {
		return nil
	}

	service, err := s.getService(namespace, name)
	if err != nil {
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	log.Infof("%s %s", meth, key)
	if service == nil {
		log.Infof("%s removing item %s", meth, key)
		s.msgChn <- NewRemoveConnectorMessage(name)
		return nil
	}

	if service.GetDeletionTimestamp().IsZero() == false {
		// The service object exists, but it has been marked to be removed, so its
		// content is not relevant.
		return nil
	}

	connector, found, err := s.getConnectorAnnotation(service)
	if err != nil {
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	if !found {
		return nil
	}

	log.Infof("%s adding item %s", meth, key)
	s.msgChn <- NewAddConnectorMessage(name, connector)
	return nil
}

// getService retrieve the kub service object from the workqueue key
// If service is not found, nil is returned
func (s *ServiceController) getService(
	namespace, name string,
) (
	service *corev1.Service, err error,
) {
	meth := s.Name + ".getService() "
	service, err = s.serviceLister.Services(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			err = nil
			service = nil
		} else {
			log.Errorf("%s %s", meth, err.Error())
		}
		return
	}
	return
}

// getConnectorAnnotation extracts the connector annotation from the kubeservice
// Returns nil if annotation doesn't exists
func (s *ServiceController) getConnectorAnnotation(
	service *corev1.Service,
) (
	connector Connector, found bool, err error,
) {
	annotation, found := service.Annotations["kumori/connector.data"]
	if !found || annotation == "" {
		return
	}
	err = json.Unmarshal([]byte(annotation), &connector)
	return
}
