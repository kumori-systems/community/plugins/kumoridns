/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// getKubeConfig loads a REST Config, depending of the environment and flags:
// Config precedence:
// * --kubeconfig flag pointing at a file
// * KUBECONFIG environment variable pointing at a file
// * In-cluster config if running in cluster
// * $HOME/.kube/config if exists (default location in the user's home directory)
func getKubeConfig(kubeConfigStr string) (config *rest.Config, err error) {
	meth := "getKubeConfig()"
	if len(kubeConfigStr) > 0 {
		log.Infof("%s Using parameter: %s", meth, kubeConfigStr)
		return clientcmd.BuildConfigFromFlags("", kubeConfigStr)
	}
	if len(os.Getenv("KUBECONFIG")) > 0 {
		log.Infof("%s Using KUBECONFIG: %s", meth, os.Getenv("KUBECONFIG"))
		return clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if c, err := rest.InClusterConfig(); err == nil {
		log.Infof("%s Using InCluster config", meth)
		return c, nil
	}
	if usr, err := user.Current(); err == nil {
		log.Infof("%s Using default .kube/config", meth)
		c, err := clientcmd.BuildConfigFromFlags("", filepath.Join(usr.HomeDir, ".kube", "config"))
		if err == nil {
			return c, nil
		}
	}
	return nil, fmt.Errorf("could not locate a kubeconfig")
}
