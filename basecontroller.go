/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kumoridns

// This code is very similar to ...
//   https://gitlab.com/kumori/platform/controllers/libraries/base-controller
// ... but it is simpler.
//
// KumoriDNS just need be aware of changes in kube-services (servicecontroller.go)
// and kumori-v3deps (v3depcontroller.go). They are the "primary resources"
// and there are no "secondary resources" (nothing to be modified)
//
// But: why not simply use the original base-controller, specifying that there
// are no secondary resources?
// - The most important reason is because KumoriDNS need to be aware of resource
//   deletions (see "setupHandlers" function), and the original BaseController
//   doesn't notify deletions.
// - Other minor reason: using the same log that the rest of CoreDNS
// - Other minor-minor reason (really, is not a reason): simplified code by
//   removing the parts related to secondary resources

import (
	"fmt"
	"time"

	wait "k8s.io/apimachinery/pkg/util/wait"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

const (
	ReasonResourceSynced         = "Synced"
	ReasonErrResourceExists      = "ErrResourceExists"
	MessageErrResourceExists     = "Resource %q already exists and is not managed by this controller"
	MessageResourceSyncedCreated = "%q synced successfully (created)"
	MessageResourceSyncedUpdated = "%q synced successfully (updated)"
	MessageResourceSyncedDeleted = "%q synced successfully (deleted)"
)

type IBaseController interface {
	GetName() string
	Run(int, <-chan struct{}, chan<- error)
	SyncHandler(string) error
	EnqueueObject(interface{})
	runWorker()
	processNextWorkItem() bool
	setupHandlers()
}

type BaseController struct {
	IBaseController
	Name                   string
	Kind                   string
	Workqueue              workqueue.RateLimitingInterface
	SyncedFunctions        []cache.InformerSynced
	PrimarySharedInformers []cache.SharedIndexInformer
}

func (c *BaseController) Run(threadiness int, stopCh <-chan struct{}, errCh chan<- error) {
	meth := c.Name + ".Run()"
	log.Info(meth)
	defer c.Workqueue.ShutDown()
	c.setupHandlers()

	log.Infof("%s Waiting for informer caches to sync", meth)
	ok := cache.WaitForCacheSync(stopCh, c.SyncedFunctions...)
	if !ok {
		errCh <- fmt.Errorf("%s failed to wait for caches to sync", meth)
		return
	}

	log.Infof("%s Starting workers", meth)
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	log.Infof("%s Shutting down", meth)
	return
}

func (c *BaseController) setupHandlers() {
	meth := c.Name + ".setupHandlers()"
	log.Infof("%s Setting handlers", meth)
	for _, sharedInformer := range c.PrimarySharedInformers {
		sharedInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
			AddFunc: c.EnqueueObject,
			UpdateFunc: func(old, new interface{}) {
				c.EnqueueObject(new)
			},
			DeleteFunc: c.EnqueueObject,
		})
	}
}

func (c *BaseController) EnqueueObject(obj interface{}) {
	meth := c.Name + ".EnqueueObject()"
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		log.Warningf("%s %v", meth, err)
		return
	}
	log.Debugf("%s key = %s", meth, key)
	c.Workqueue.Add(key)
}

func (c *BaseController) runWorker() {
	for c.processNextWorkItem() {
	}
}

func (c *BaseController) processNextWorkItem() bool {
	meth := c.Name + ".processNextWorkItem()"

	obj, shutdown := c.Workqueue.Get()
	if shutdown {
		return false
	}

	err := func(obj interface{}) error {
		defer c.Workqueue.Done(obj)

		var key string
		var ok bool
		if key, ok = obj.(string); !ok {
			c.Workqueue.Forget(obj)
			log.Warningf("%s expected string in Workqueue but got %v", meth, obj)
			return nil
		}

		log.Debugf("%s Calling SyncHandler()", meth)
		if err := c.SyncHandler(key); err != nil {
			log.Warningf("%s error syncing %v", meth, err)
			c.Workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %v, requeuing", key, err)
		}

		c.Workqueue.Forget(obj)
		return nil
	}(obj)

	if err != nil {
		log.Warningf("%s error %v", meth, err)
		return true
	}
	return true
}
