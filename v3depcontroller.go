/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"fmt"
	"sort"

	kukuv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukuinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kukulisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

//
// V3DeploymentController detects changes in the Kubernetes v3deployments, and
// sends (via go channel) relevant information to the Translation table.
// What is that relevant information? For each role, its list of connected
// client channels (see roles.go), ordered alphabetically
//

const kindV3Deployment = "V3Deployment"

// V3DeploymentController is the struct representing the controller
type V3DeploymentController struct {
	BaseController
	kukuClientset        kukuclientset.Interface
	v3DeploymentInformer kukuinformers.V3DeploymentInformer
	v3DeploymentLister   kukulisters.V3DeploymentLister
	namespace            string
	msgChn               chan RolesMessage
	threadiness          int
}

// NewV3DeploymentController creates a new controller for kube-services, used in the
// kumoridns plugin (coredns plugin)
func NewV3DeploymentController(
	kukuClientset kukuclientset.Interface,
	v3DeploymentInformer kukuinformers.V3DeploymentInformer,
	namespace string,
	msgChn chan RolesMessage,
	threadiness int,
) (s *V3DeploymentController) {

	controllerName := "kumoridnsv3deploymentcontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 1)
	syncedFunctions[0] = v3DeploymentInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = v3DeploymentInformer.Informer()

	v3DeploymentLister := v3DeploymentInformer.Lister()

	baseController := BaseController{
		Name:                   controllerName,
		Kind:                   kindKubeService,
		Workqueue:              workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		SyncedFunctions:        syncedFunctions,
		PrimarySharedInformers: primarySharedInformers,
	}

	s = &V3DeploymentController{
		BaseController:       baseController,
		kukuClientset:        kukuClientset,
		v3DeploymentInformer: v3DeploymentInformer,
		v3DeploymentLister:   v3DeploymentLister,
		namespace:            namespace,
		msgChn:               msgChn,
		threadiness:          threadiness,
	}
	s.BaseController.IBaseController = s
	return
}

// SyncHandler ...
func (s *V3DeploymentController) SyncHandler(key string) error {
	meth := s.Name + ".SyncHandler() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		err = fmt.Errorf("Invalid resource key: %s", key)
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	if s.namespace != "" && s.namespace != namespace {
		return nil
	}

	v3dep, err := s.getV3Deployment(namespace, name)
	if err != nil {
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	log.Infof("%s %s", meth, key)
	if v3dep == nil {
		log.Infof("%s removing item %s", meth, key)
		s.msgChn <- NewRemoveRolesMessage(name)
		return nil
	}

	roles, err := s.getLinkedClientChannelsPerRole(v3dep)
	if err != nil {
		log.Warningf("%s error %v", meth, err)
		return nil
	}

	log.Infof("%s adding item %s", meth, key)
	s.msgChn <- NewAddRolesMessage(v3dep.Name, roles)

	return nil
}

// getV3Deployment retrieve the v3deployment object from the workqueue key
// If v3deployment is not found, nil is returned
func (s *V3DeploymentController) getV3Deployment(
	namespace, name string,
) (
	v3dep *kukuv1.V3Deployment, err error,
) {
	meth := s.Name + ".getV3Deployment() "
	v3dep, err = s.v3DeploymentLister.V3Deployments(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			err = nil
			v3dep = nil
		} else {
			log.Errorf("%s %s", meth, err.Error())
		}
		return
	}
	return
}

// getLinkedClientChannelsPerRole extracts the linked client channels of each
// role. Channels are ordered by name
func (s *V3DeploymentController) getLinkedClientChannelsPerRole(
	v3dep *kukuv1.V3Deployment,
) (
	roles Roles, err error,
) {

	if v3dep.Spec.Artifact == nil ||
		v3dep.Spec.Artifact.Description == nil ||
		v3dep.Spec.Artifact.Description.Roles == nil ||
		v3dep.Spec.Artifact.Description.Connectors == nil {
		return
	}

	service := *v3dep.Spec.Artifact.Description

	roles = Roles{}
	for name := range *service.Roles {
		roles[name] = ClientChannels{}
	}

	for _, connector := range *service.Connectors {
		if connector.Clients != nil {
			for _, client := range connector.Clients {
				if client.Role == "self" { // Special role for service channels
					continue
				}
				_, found := roles[client.Role]
				if found {
					roles[client.Role] = append(roles[client.Role], client.Channel)
				}
			}
		}
	}

	for _, channels := range roles {
		sort.Strings(channels)
	}

	return
}
