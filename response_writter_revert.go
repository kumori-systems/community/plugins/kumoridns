/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"github.com/miekg/dns"
)

// ResponseWriterRevert set the original name (before it was rewritten) in the
// response question.
// This is need because the client will otherwise disregards the response!

type ResponseWriterRevert struct {
	dns.ResponseWriter
	originalQuestion dns.Question
	originalName     string
}

// NewResponseRewrite returns a pointer to a new ResponseRewrite.
func NewResponseWriterRevert(
	w dns.ResponseWriter, r *dns.Msg, originalName string,
) *ResponseWriterRevert {
	return &ResponseWriterRevert{
		ResponseWriter:   w,
		originalQuestion: r.Question[0],
		originalName:     originalName,
	}
}

// WriteMsg records the status code and calls the underlying ResponseWriter's
// WriteMsg method.
func (r *ResponseWriterRevert) WriteMsg(res *dns.Msg) error {
	res.Question[0] = r.originalQuestion
	for _, rr := range res.Answer {
		rr.Header().Name = r.originalName
	}
	return r.ResponseWriter.WriteMsg(res)
}

// Write is a wrapper that records the size of the message that gets written.
func (r *ResponseWriterRevert) Write(buf []byte) (int, error) {
	n, err := r.ResponseWriter.Write(buf)
	return n, err
}
