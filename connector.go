/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

//
// ServiceController sendsdetects changes in the Kubernetes services, and sends
// (via go channel) relevant information to the Translation table.
// What is that relevant information? The content of the "connector" annotation,
// included in each service (see connector.go)
//

// ConnectorMessageType is the list of allowed operations with connectors
type ConnectorMessageType string

const (
	// AddConnMessage is a message to add/update a connector to the translation table
	AddConnMessage ConnectorMessageType = "AddConn"
	// RemoveConnMessage is a message to remove a connector to the translation table
	RemoveConnMessage = "RemoveConn"
)

// Endpoint describes a source/target endpoint. Role not exists for service
// endpoints
type Endpoint struct {
	Role    *string `json:"role"`
	Channel string  `json:"channel"`
}

// Connector contains information of a given service connector
type Connector struct {
	Name       string     `json:"name"`
	Tag        string     `json:"tag"`
	Kind       string     `json:"kind"`
	Service    string     `json:"service"`
	Deployment string     `json:"deployment"`
	Clients    []Endpoint `json:"clients"`
	Servers    []Endpoint `json:"servers"`
	Duplex     []Endpoint `json:"duplex"`
}

// ConnectorMessage describes an add/remove operation
type ConnectorMessage struct {
	Operation ConnectorMessageType
	Service   string
	Connector Connector
}

// NewAddConnectorMessage ...
func NewAddConnectorMessage(srv string, conn Connector) ConnectorMessage {
	return ConnectorMessage{
		Operation: AddConnMessage,
		Service:   srv,
		Connector: conn,
	}
}

// NewRemoveConnectorMessage ...
func NewRemoveConnectorMessage(srv string) ConnectorMessage {
	return ConnectorMessage{
		Operation: RemoveConnMessage,
		Service:   srv,
		Connector: Connector{}, // empty connector
	}
}

// GetDomainNamePairs calculate and return de list of pairs local-domain-name/
// global-domain name related to the connector
//
// i=tag, c=channel, C=connector, R=role, D=deploy
// Being <D-C-i> a hashed value in the form `kd-xxxxxx-xxxxxxxx-xxxxxxxx`
//
// CLIENT CHANNELS (connector can be LB or full)
//
// - Local = i.c.R.D.kumori.dep.cluster.local, being c=client and LB connector
//   Global = <D-C-i>-lb-service.kumori.svc.cluster.local
//
// - Local = i.c.R.D.kumori.dep.cluster.local, being c=client and full connector
//   Global = <D-C-i>-service.kumori.svc.cluster.local
//
// - Local = set.i.c.R.D.kumori.dep.cluster.local, being c=client
//   Global = <D-C-i>-service.kumori.svc.cluster.local
//
// - Local = ready.i.c.R.D.kumori.dep.cluster.local, being c=client
//   Global = <D-C-i>-ready-service.kumori.svc.cluster.local
//
// DUPLEX CHANNELS (connector must be full)
//
// - Local = c.R.D.kumori.dep.cluster.local, being c=duplex
//   Global = <D-C-i>-service.kumori.svc.cluster.local
//
// - Local = ready.c.R.D.kumori.dep.cluster.local, being c=duplex
//   Global = <D-C-i>-ready-service.kumori.svc.cluster.local
//
// Example of annotation content:
// {
//   "name": "lbconnector",
//   "tag": "1",
//   "kind": "lb",
//   "service": "service_cache_tags",
//   "deployment": "kd-090147-3d8b8e51",
//   "clients": [
//     {
//       "role": "frontend",
//       "channel": "restapiclient"
//     }
//   ],
//   "servers": [
//     {
//       "role": "worker",
//       "channel": "restapiserver",
//       "port": 8080
//     }
//   ],
//   "linkedClients": [],
//   "linkedServers": [],
//   "duplex": []
// }
//
func (c *Connector) GetDomainNamePairs(
	namespace string, internalDomain string,
) (
	pairs []DomainNamePair,
) {

	// Suffixes
	srcSuffix := "." + namespace + ".dep." + internalDomain + "."
	dstSuffix := "." + namespace + ".svc." + internalDomain + "."

	// Allowed destinations: kube services related to the deployment-connector-tag
	kubeServiceBaseName := c.Deployment + "-" + Hash(c.Name+"-"+c.Tag)
	DCi := kubeServiceBaseName + "-service" + dstSuffix
	DCiLB := kubeServiceBaseName + "-lb-service" + dstSuffix
	DCiReady := kubeServiceBaseName + "-ready-service" + dstSuffix

	// CLIENT CHANNELS (connector can be LB or full)
	for _, cli := range c.Clients {
		if cli.Role == nil { // service channels
			continue
		}
		roleNameHash := Hash(*cli.Role)
		cRD := cli.Channel + "." + roleNameHash + "." + c.Deployment + srcSuffix
		icRD := c.Tag + "." + cRD
		if c.Kind == "lb" {
			// Local = i.c.R.D.kumori.dep.cluster.local, being c=client and LB connector
			// Global = <D-C-i>-lb.kumori.svc.cluster.local
			pairs = append(pairs, DomainNamePair{
				LocalDomainName:  icRD,
				GlobalDomainName: DCiLB,
			})
		} else {
			// Local = i.c.R.D.kumori.dep.cluster.local, being c=client and full connector
			// Global = <D-C-i>.kumori.svc.cluster.local
			pairs = append(pairs, DomainNamePair{
				LocalDomainName:  icRD,
				GlobalDomainName: DCi,
			})
		}
		// Local = set.i.c.R.D.kumori.dep.cluster.local, being c=client
		// Global = <D-C-i>.kumori.svc.cluster.local
		pairs = append(pairs, DomainNamePair{
			LocalDomainName:  "set." + icRD,
			GlobalDomainName: DCi,
		})
		// Local = ready.i.c.R.D.kumori.dep.cluster.local, being c=client
		// Global = <D-C-i>-ready.kumori.svc.cluster.local
		pairs = append(pairs, DomainNamePair{
			LocalDomainName:  "ready." + icRD,
			GlobalDomainName: DCiReady,
		})
	}

	// DUPLEX CHANNELS (connector must be full, and the tag is its own tag)
	for _, dup := range c.Duplex {
		if dup.Role == nil { // service channels
			continue
		}
		roleNameHash := Hash(*dup.Role)
		cRD := dup.Channel + "." + roleNameHash + "." + c.Deployment + srcSuffix
		// Local = c.R.D.kumori.dep.cluster.local, being c=duplex
		// Global = <D-C-i>.kumori.svc.cluster.local
		pairs = append(pairs, DomainNamePair{
			LocalDomainName:  cRD,
			GlobalDomainName: DCi,
		})
		// Local = ready.c.R.D.kumori.dep.cluster.local, being c=duplex
		// Global = <D-C-i>-ready.kumori.svc.cluster.local
		pairs = append(pairs, DomainNamePair{
			LocalDomainName:  "ready." + cRD,
			GlobalDomainName: DCiReady,
		})
	}

	return
}
