/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

// Translator allows get the destination for a source.
// Tranlator talks with:
// - kumoridns, via RequestChn and ResponseChn, to resolve a source
// - kubecontroller, vía connMsgChn, to get changes
// - RestAPI, via RestapiReqChn and RestapiResChan, to get the tranlator dictionary
//   for debug purposes
type Translator struct {
	namespace      string
	internalDomain string
	sidecarIPRange []string
	dictionary     *Dictionary
	connMsgChn     chan ConnectorMessage
	rolesMsgChn    chan RolesMessage
	RequestChn     chan string
	ResponseChn    chan string
	RestapiReqChn  chan string
	RestapiResChn  chan []DomainNamePair
	stopChn        chan bool
}

// NewTranslator creates a new empty Translator object
func NewTranslator(
	namespace string,
	internalDomain string,
	sidecarCIDR string,
	connMsgChn chan ConnectorMessage,
	rolesMsgChn chan RolesMessage,
	requestChn chan string,
	responseChn chan string,
	restapiReqChn chan string,
	restapiResChn chan []DomainNamePair,
) (
	t *Translator, err error,
) {
	sidecarIPRange, err := getCIDRRange(sidecarCIDR)
	if err != nil {
		log.Warningf("Translator.NewTranslator Error getting range for sidecarCIDR %s:%s", sidecarCIDR, err.Error())
		return
	}
	t = &Translator{
		namespace:      namespace,
		internalDomain: internalDomain,
		sidecarIPRange: sidecarIPRange,
		dictionary:     NewDictionary(),
		connMsgChn:     connMsgChn,
		rolesMsgChn:    rolesMsgChn,
		RequestChn:     requestChn,
		ResponseChn:    responseChn,
		RestapiReqChn:  restapiReqChn,
		RestapiResChn:  restapiResChn,
		stopChn:        make(chan bool),
	}
	return
}

// Start initializes the translation dictionary
func (t *Translator) Start() {
	go t.loop()
	return
}

// Stop finalizes the translation dictionary
func (t *Translator) Stop() {
	t.stopChn <- true
	return
}

// loop implements the loop receiving messages from Kubernetes controller
// and DNS requests
func (t *Translator) loop() {
	meth := "Translator.loop()"

	for {
		select {

		// Received a message with information about a connector created/updated/removed
		case connMsg := <-t.connMsgChn:
			if connMsg.Operation == AddConnMessage {
				log.Infof("%s Kubernetes add/update entries for service:%s", meth, connMsg.Service)
				t.addEntries(connMsg.Service, connMsg.Connector.GetDomainNamePairs(t.namespace, t.internalDomain))
			} else if connMsg.Operation == RemoveConnMessage {
				log.Infof("%s Kubernetes remove entries for service:%s", meth, connMsg.Service)
				t.removeEntries(connMsg.Service)
			} else {
				log.Infof("%s Kubernetes unknown operation:%s", meth, connMsg.Operation)
			}

		// Received a message with information about a v3deployment created/updated/removed
		case rolesMsg := <-t.rolesMsgChn:
			if rolesMsg.Operation == AddRolesMessage {
				log.Infof("%s Kubernetes add/update entries for deployment:%s", meth, rolesMsg.Deployment)
				t.addEntries(
					rolesMsg.Deployment,
					rolesMsg.Roles.GetDomainNamePairs(t.namespace, t.internalDomain, rolesMsg.Deployment, t.sidecarIPRange),
				)
			} else if rolesMsg.Operation == RemoveRolesMessage {
				log.Infof("%s Kubernetes remove entries for deployment:%s", meth, rolesMsg.Deployment)
				t.removeEntries(rolesMsg.Deployment)
			} else {
				log.Infof("%s Kubernetes unknown operation:%s", meth, rolesMsg.Operation)
			}

		// Received a DNS request
		case request := <-t.RequestChn:
			response := t.translate(request)
			log.Infof("%s DNS Request:%s, Response:%s", meth, request, response)
			t.ResponseChn <- response

		// Received an APIRest request (just for debug purposes)
		case apirequest := <-t.RestapiReqChn:
			log.Infof("%s RestAPI Request:%s", meth, apirequest)
			t.RestapiResChn <- t.getPairs(apirequest)

		case <-t.stopChn:
			log.Infof("%s Stop signal", meth)
			break
		}
	}
}

// addEntries updates the translator dictionary when a new service/deployment
// is received, or when a service/deployment is udpated
// Note: the service/deployment is the owner param
func (t *Translator) addEntries(owner string, pairs []DomainNamePair) {
	t.removeEntries(owner) // Maybe this is an update operation, so remove old entries
	for _, pair := range pairs {
		t.dictionary.AddEntry(pair, owner)
	}
	return
}

// removeEntries updates de translator dictionary when a service/deployment is removed
// Note: the service/deployment is the owner param
func (t *Translator) removeEntries(owner string) {
	t.dictionary.RemoveOwner(owner)
	return
}

// getPairs return the translator dictionary in a simple way (list of pairs src->dst)
func (t *Translator) getPairs(owner string) (pairs []DomainNamePair) {
	pairs = []DomainNamePair{}
	for localDomainName, entry := range t.dictionary.Data {
		if owner == "all" || entry.ContainsOwner(owner) {
			pairs = append(pairs, DomainNamePair{localDomainName, entry.GetGlobalDmainName()})
		}
	}
	return
}

// Translate ...
func (t *Translator) translate(request string) (response string) {
	return t.dictionary.Translate(request)
}
