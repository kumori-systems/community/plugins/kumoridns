/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"fmt"
	"time"

	"github.com/coredns/caddy"
	"github.com/coredns/coredns/core/dnsserver"
	"github.com/coredns/coredns/plugin"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukuinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes"
)

var serviceController *ServiceController
var v3DeploymentController *V3DeploymentController
var translator *Translator
var restapi *RestAPI

const defaultSidecarCIDR = "127.0.60.0/24"
const threadiness int = 1
const resyncInterval = time.Duration(0)

// init registers this plugin.
func init() {
	plugin.Register("kumoridns", setup)
}

// setup is the function that gets called when the config parser see the token
// "kumoridns". Setup is responsible for parsing any extra options the kumoridns
// plugin may have. The first token this function sees is "kumoridns".
func setup(c *caddy.Controller) error {
	meth := "setup()"
	log.Infof("%s init", meth)

	// namespace to take into account (for example: "kumori")
	namespace := ""

	// Internal Kubernetes domain (for example: "cluster.local")
	internalDomain := ""

	// Range of IPs used for sidecars
	sidecarCIDR := defaultSidecarCIDR

	// kubernetes config. Default: In-cluster config if running in cluster,
	// or $HOME/.kube/config.
	kubeConfigStr := ""

	// Parse parameters
	c.Next() // The first parameter is the plugin name
	if !c.Next() {
		err := fmt.Errorf("Mandatory parameter namespace not found")
		log.Errorf("%s ", meth, err.Error())
		return err
	}
	namespace = c.Val()
	if !c.Next() {
		err := fmt.Errorf("Mandatory parameter internaldomain not found")
		log.Errorf("%s ", meth, err.Error())
		return err
	}
	internalDomain = c.Val()
	if c.Next() {
		sidecarCIDR = c.Val()
		if c.Next() {
			kubeConfigStr = c.Val()
		}
	}

	log.Infof(
		"%s Parameters: namespace=%s, internalDomain=%s, sidecarCIDR=%s, kubeconfig=%s",
		meth, namespace, internalDomain, sidecarCIDR, kubeConfigStr,
	)

	// This channel allows the ServiceController send information to the
	// Translator about the connectors
	connMsgChn := make(chan ConnectorMessage)
	// This channel allows the V3DepController send information to the
	// Translator about the roles
	rolesMsgChn := make(chan RolesMessage)
	// These channels allow the plugin consult the Translator
	requestChn := make(chan string)
	responseChn := make(chan string)
	// These channels allow the restapi consult the Translator (debugging purposes)
	restapiReqChn := make(chan string)
	restapiResChn := make(chan []DomainNamePair)

	//
	// Create the Translator
	//
	translator, err := NewTranslator(
		namespace, internalDomain, sidecarCIDR,
		connMsgChn, rolesMsgChn, requestChn, responseChn,
		restapiReqChn, restapiResChn,
	)
	if err != nil {
		return err
	}

	//
	// Create the ServiceController and V3DepController
	//
	kubeConfig, err := getKubeConfig(kubeConfigStr)
	if err != nil {
		return err
	}
	kubeClientset, err := kubeclientset.NewForConfig(kubeConfig)
	if err != nil {
		return err
	}
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset, resyncInterval, "", func(opt *metav1.ListOptions) {},
	)
	serviceController := NewServiceController(
		kubeClientset,
		kubeInformerFactory.Core().V1().Services(),
		namespace,
		connMsgChn,
		threadiness,
	)
	kukuClientset, err := kukuclientset.NewForConfig(kubeConfig)
	if err != nil {
		return err
	}
	kukuInformerFactory := kukuinformers.NewFilteredSharedInformerFactory(
		kukuClientset, resyncInterval, "", func(opt *metav1.ListOptions) {},
	)
	v3DepController := NewV3DeploymentController(
		kukuClientset,
		kukuInformerFactory.Kumori().V1().V3Deployments(),
		namespace,
		rolesMsgChn,
		threadiness,
	)

	//
	// Create the RestAPI
	//
	restapi := NewRestAPI(":9154", translator)

	// What to do when the plugin is launched
	c.OnStartup(func() error {
		log.Infof("%s OnStartup", meth)
		if err != nil {
			log.Errorf("%s OnStartup", meth, err.Error())
			return err
		}

		//
		// Start translator
		//
		translator.Start()

		//
		// Start ServiceController and V3DeploymentController
		//
		stopCh := SetupStopSignal()
		errCh := make(chan error)
		kukuInformerFactory.Start(stopCh)
		go serviceController.Run(serviceController.threadiness, stopCh, errCh)
		kubeInformerFactory.Start(stopCh)
		go v3DepController.Run(v3DepController.threadiness, stopCh, errCh)
		go func() {
			select {
			case err := <-errCh: // Detect errors starting controllers
				log.Infof("%s Error running controller: %s", meth, err.Error())
			case <-stopCh: // Wait application closed
				log.Infof("%s Closing controller", meth)
			}
		}()

		//
		// Start restapi
		//
		err := restapi.Start()
		if err != nil {
			log.Errorf("%s OnStartup", meth, err.Error())
			return err
		}

		log.Infof("%s OnStartup done", meth)
		return nil
	})

	// What to do when the plugin is shutdown
	c.OnShutdown(func() error {
		log.Infof("%s OnShutdown", meth)
		if restapi != nil {
			err := restapi.Stop()
			if err != nil { // Just log the error
				log.Errorf("%s OnShutdown", meth, err.Error())
			}
		}
		if translator != nil {
			translator.Stop()
		}
		log.Infof("%s OnShutdown done", meth)
		return nil
	})

	// Add the Plugin to CoreDNS, so Servers can use it in their plugin chain.
	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		return KumoriDNS{
			Next:       next,
			translator: translator,
		}
	})

	// All OK, return a nil error.
	return nil
}
