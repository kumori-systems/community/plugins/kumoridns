#!/bin/bash

source gitlab-ci-helper/setenv-coredns-image-tag.sh

COREDNS_REPO="https://github.com/coredns/coredns.git"
COREDNS_VERSION="v${COREDNS_IMAGE_TAG}"
KUMORI_CLIENTGO_PACKAGE="gitlab.com/kumori-systems/community/libraries/client-go"
KUMORI_CLIENTGO_VERSION="v1.5.0"

# Just for dev purposes
USE_PRIVATE_KUMORI_CLIENTGO="false"
DEV_KUMORI_CLIENTGO_PACKAGE="gitlab.com/kumori/platform/controllers/libraries/client-go.git"
DEV_KUMORI_CLIENTGO_VERSION="v1.5.0-ticket1276"

echo "Remove CoreDNS directory, if exists"
if [ -d "./coredns" ]; then rm -Rf ./coredns; fi

echo "Cloning CoreDNS"
git clone $COREDNS_REPO

echo "Checkout to appropiate version ${COREDNS_VERSION}"
cd coredns
git checkout $COREDNS_VERSION

# Add a require section with the Kumori client-go package
REQUIRE_DIRECTIVE="\nrequire (\n\t%s %s\n)\n"
printf "${REQUIRE_DIRECTIVE}" "${KUMORI_CLIENTGO_PACKAGE}" "${KUMORI_CLIENTGO_VERSION}" >> go.mod

#
# Just for dev purposes.
# Uncomment when it is required to use a private client-go.
# GO*** env variables are setted to avoid the "410 gone" problem during go-get
#
if [ "${USE_PRIVATE_KUMORI_CLIENTGO}" = "true" ]; then
  export GO111MODULE=on
  export GOPROXY=direct
  export GOSUMDB=off
  REPLACE_DIRECTIVE="\nreplace %s %s => %s %s\n"
  printf "${REPLACE_DIRECTIVE}" "${KUMORI_CLIENTGO_PACKAGE}" "${KUMORI_CLIENTGO_VERSION}" "${DEV_KUMORI_CLIENTGO_PACKAGE}" "${DEV_KUMORI_CLIENTGO_VERSION}" >> go.mod
fi

echo "Update the go.mod file to include the Kumori client-go dependency"

echo "In-tree plugin: copying kumoridns into CoreDNS project"
mkdir ./plugin/kumoridns
cp ../*.go ./plugin/kumoridns

echo "Adding kumoridns plugin to plugins.cfg"
sed 's/kubernetes:kubernetes/kumoridns:kumoridns\nkubernetes:kubernetes/' ./plugin.cfg >> ./tmp.cfg
mv ./tmp.cfg ./plugin.cfg

echo "Executing go mod tidy"
# Why "-compat=1.17" flag? Because:
# - https://stackoverflow.com/questions/71973152/go-mod-tidy-error-message-but-go-1-16-would-select
# - https://go.dev/ref/mod#graph-pruning
rm go.sum
go mod tidy -compat=1.17

# "go generate" is required to regenerate code in files core/plugin/zplugin.go
# and core/dnsserver/zdirectives.go: plugin.cfg
echo "Executing go-generate (this operation this operation can take several minutes, as it downloads different packages)"
go generate -v

cd ..
