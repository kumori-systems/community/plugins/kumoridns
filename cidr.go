/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import "net"

// CIDR utilities

func getCIDRRange(cidr string) (IPRange []string, err error) {
	IPRange = []string{}
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return
	}
	firstIP := true // Avoid first IP (x.x.x.0)
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		if firstIP {
			firstIP = false
		} else {
			IPRange = append(IPRange, ip.String())
		}
	}
	return
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
