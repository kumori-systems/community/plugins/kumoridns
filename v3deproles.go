/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

// RolesMessageType is the list of allowed operations with v3deps
type RolesMessageType string

const (
	// AddRolesMessage is a message to add/update a v3dep to the translation table
	AddRolesMessage RolesMessageType = "AddRoles"
	// RemoveRolesMessage is a message to remove a v3dep to the translation table
	RemoveRolesMessage = "RemoveRoles"
)

// ClientChannels contains the list of client channels of a role
type ClientChannels []string

// Roles is a map: key is the role name, and value is a list of client channel names
type Roles map[string]ClientChannels

// RolesMessage describes an add/remove operation
type RolesMessage struct {
	Operation  RolesMessageType
	Deployment string
	Roles      Roles
}

// NewAddRolesMessage ...
func NewAddRolesMessage(depName string, roles Roles) RolesMessage {
	return RolesMessage{
		Operation:  AddRolesMessage,
		Deployment: depName,
		Roles:      roles,
	}
}

// NewRemoveRolesMessage ...
func NewRemoveRolesMessage(depName string) RolesMessage {
	return RolesMessage{
		Operation:  RemoveRolesMessage,
		Deployment: depName,
		Roles:      Roles{}, // empty
	}
}

// GetDomainNamePairs calculate and return de list of pairs local-domain-name/
// global-domain name related to the roles of v3deployment
//
// CLIENT CHANNELS (connector can be LB or full)
// - Local = c.R.D.kumori.dep.cluster.local, being c=client
//   Global = 127.0.60.1 (or other, it is a sidecar local IP)
//
// DUPLEX CHANNELS
// - Not applicable
//
// c=channel, R=role, D=deploy
//
func (r *Roles) GetDomainNamePairs(
	namespace string, internalDomain string, deployment string, sidecarIPRange []string,
) (
	pairs []DomainNamePair,
) {
	for roleName, clientChannels := range *r {
		index := 0
		srcSuffix := "." + namespace + ".dep." + internalDomain + "."
		roleNameHash := Hash(roleName)
		for _, channel := range clientChannels {
			cRD := channel + "." + roleNameHash + "." + deployment + srcSuffix
			pairs = append(pairs, DomainNamePair{
				LocalDomainName:  cRD,
				GlobalDomainName: sidecarIPRange[index],
			})
			index++
		}
	}
	return
}
