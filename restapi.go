/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import (
	"io"
	"net"
	"net/http"
	"sync"

	"github.com/coredns/coredns/plugin/pkg/reuseport"
	"github.com/coredns/coredns/plugin/pkg/uniq"
)

// -----------------------------------------------------------------------------
//
// This is a RestAPI (caddy implementation) for debugging purposes.
//
// Based in plugin/ready implementation.
//
// -----------------------------------------------------------------------------

var uniqAddr = uniq.New()

// RestAPI ....
type RestAPI struct {
	Addr       string
	translator *Translator
	sync.RWMutex
	ln   net.Listener
	done bool
	mux  *http.ServeMux
}

// NewRestAPI ....
func NewRestAPI(addr string, translator *Translator) *RestAPI {
	return &RestAPI{
		Addr:       addr,
		translator: translator,
	}
}

// Start initializes the RestAPI
func (r *RestAPI) Start() error {
	ln, err := reuseport.Listen("tcp", r.Addr)
	if err != nil {
		return err
	}

	r.Lock()
	r.ln = ln
	r.mux = http.NewServeMux()
	r.done = true
	r.Unlock()

	r.mux.HandleFunc("/dictionary", func(w http.ResponseWriter, req *http.Request) {
		owner := "all"
		owners, ok := req.URL.Query()["owner"]
		if ok && len(owners[0]) > 0 {
			owner = owners[0]
		}

		r.translator.RestapiReqChn <- owner
		response := <-r.translator.RestapiResChn
		responsestr := ""
		for _, v := range response {
			responsestr = responsestr + v.LocalDomainName + " => " + v.GlobalDomainName + "\n"
		}
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, responsestr)
		return
	})

	go func() { http.Serve(r.ln, r.mux) }()

	return nil
}

// Stop finalizes the RestAPI
func (r *RestAPI) Stop() error {
	r.Lock()
	defer r.Unlock()
	if !r.done {
		return nil
	}

	uniqAddr.Unset(r.Addr)

	r.ln.Close()
	r.done = false
	return nil
}
