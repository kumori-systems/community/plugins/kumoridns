/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package kumoridns is a CoreDNS plugin implementing the Kumori Service Discovery
//
package kumoridns

import "sync"

// Dictionary is a map where:
// - The key is a local domain name
//   For example: i.c.R.D.kumori.dep.cluster.local
// - The value is a global domain name, plus the "owners" of that entry.
//   For example: C-i-D-lb.kumori.svc.cluster.local + owners
//   The owners of a entry can be:
//   - The kube services related to the connectors related to this translation.
//     Remember!: a kumori connector can have several kube services
//   - The v3deployment related to this translation (sidecar IPs case)
// An entry in the dictionary is removed only when all its owners are removed
type Dictionary struct {
	Data map[string]*Entry
	mux  sync.Mutex
}

// Entry contains a global domain name (a translation result), plus the "owners"
// of that entry.
// The owners of a entry can be:
// - The kube services related to the connectors related to this translation.
// - The v3deployment related to this translation (sidecar IPs case)
type Entry struct {
	GlobalDomainName string
	Owners           []string
}

// DomainNamePair contains a pair of a local domain and its corresponding
// global domain name (translation result).
// This object is used to send the information pair through go channels
// For example:
//   Local = i.c.R.D.kumori.dep.cluster.local
//   Global = C-i-D-lb.kumori.svc.cluster.local
type DomainNamePair struct {
	LocalDomainName  string
	GlobalDomainName string
}

//------------------------------------------------------------------------------
// Dictionary methods
//------------------------------------------------------------------------------

func NewDictionary() *Dictionary {
	return &Dictionary{
		Data: make(map[string]*Entry),
	}
}

func (tb *Dictionary) AddEntry(domainNamePair DomainNamePair, owner string) {
	tb.mux.Lock()
	local := domainNamePair.LocalDomainName
	global := domainNamePair.GlobalDomainName
	if entry, found := tb.Data[local]; found {
		entry.SetGlobalDomainName(global)
		if !entry.ContainsOwner(owner) {
			entry.AddOwner(owner)
		}
	} else {
		tb.Data[local] = NewEntry(global, owner)
	}
	tb.mux.Unlock()
}

func (tb *Dictionary) RemoveEntry(domainNamePair DomainNamePair, owner string) {
	tb.mux.Lock()
	local := domainNamePair.LocalDomainName
	if entry, found := tb.Data[local]; found {
		entry.RemoveOwner(owner)
		if entry.OwnersIsEmpty() {
			delete(tb.Data, local)
		}
	}
	tb.mux.Unlock()
}

func (tb *Dictionary) RemoveOwner(owner string) {
	tb.mux.Lock()
	for local, entry := range tb.Data {
		if entry.RemoveOwner(owner) {
			if entry.OwnersIsEmpty() {
				delete(tb.Data, local)
			}
		}
	}
	tb.mux.Unlock()
}

func (tb *Dictionary) Translate(localDomainName string) (globalDomainName string) {
	tb.mux.Lock()
	globalDomainName = ""
	if entry, found := tb.Data[localDomainName]; found {
		globalDomainName = entry.GetGlobalDmainName()
	}
	tb.mux.Unlock()
	return globalDomainName
}

//------------------------------------------------------------------------------
// Entry methods
//------------------------------------------------------------------------------

func NewEntry(globalDomainName string, owner string) *Entry {
	return &Entry{
		GlobalDomainName: globalDomainName,
		Owners:           []string{owner},
	}
}

func (d *Entry) SetGlobalDomainName(globalDomainName string) {
	d.GlobalDomainName = globalDomainName
}

func (d *Entry) GetGlobalDmainName() string {
	return d.GlobalDomainName
}

func (d *Entry) IndexOfOwner(owner string) int {
	for k, v := range d.Owners {
		if v == owner {
			return k
		}
	}
	return -1
}

func (d *Entry) ContainsOwner(owner string) bool {
	index := d.IndexOfOwner(owner)
	return index != -1
}

func (d *Entry) RemoveOwner(owner string) bool {
	index := d.IndexOfOwner(owner)
	if index != -1 {
		d.Owners = append(d.Owners[:index], d.Owners[index+1:]...)
		return true
	}
	return false
}

func (d *Entry) OwnersIsEmpty() bool {
	return len(d.Owners) == 0
}

func (d *Entry) AddOwner(service string) {
	d.Owners = append(d.Owners, service)
}
